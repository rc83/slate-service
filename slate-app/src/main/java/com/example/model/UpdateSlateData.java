package com.example.model;

public class UpdateSlateData {
	
	public UpdateSlateData() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String msMonitoringReqInd;
	private String msTechnicalAssistanceInd;
	private String applicationComment;
	private String grantFundingStatusCd;
	
	public String getMsMonitoringReqInd() {
		return msMonitoringReqInd;
	}
	public void setMsMonitoringReqInd(String msMonitoringReqInd) {
		this.msMonitoringReqInd = msMonitoringReqInd;
	}
	public String getMsTechnicalAssistanceInd() {
		return msTechnicalAssistanceInd;
	}
	public void setMsTechnicalAssistanceInd(String msTechnicalAssistanceInd) {
		this.msTechnicalAssistanceInd = msTechnicalAssistanceInd;
	}
	public String getApplicationComment() {
		return applicationComment;
	}
	public void setApplicationComment(String applicationComment) {
		this.applicationComment = applicationComment;
	}
	public String getGrantFundingStatusCd() {
		return grantFundingStatusCd;
	}
	public void setGrantFundingStatusCd(String grantFundingStatusCd) {
		this.grantFundingStatusCd = grantFundingStatusCd;
	}
	
	
}
