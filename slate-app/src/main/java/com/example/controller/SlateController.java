package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.ApplicationInformationEntity;
import com.example.model.UpdateSlateData;
import com.example.service.SlateService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("demo")
public class SlateController {

	@Autowired
	protected SlateService appService;


	/**
	 * Update/Save Slate information for an application.
	 * @param applicationId
	 * @param slateData
	 * @return
	 */
	@ApiOperation(value = "Update an abstract based on an abstract-id")
	@PutMapping("/process-slate/{applicationId}")
	public @ResponseBody ApplicationInformationEntity updateSlate(@PathVariable(value="applicationId") int applicationId,
			@RequestBody UpdateSlateData slateData) {
		
		return appService.updateSlateData(applicationId, slateData);
		
	}
	
}
