package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.ApplicationInformationEntity;
import com.example.exception.ResourceNotFoundException;
import com.example.model.UpdateSlateData;
import com.example.repository.AppRepository;

@Service
public class SlateService {
	
	@Autowired
	private AppRepository appRepository;
	
	/**
	 * Update the selected abstract entity.
	 */
	public ApplicationInformationEntity updateSlateData(int id, UpdateSlateData slateData) {
		// Find the Slate data for the id
		ApplicationInformationEntity updateSlateData = appRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
		
		updateSlateData.setMsMonitoringReqInd(slateData.getMsMonitoringReqInd());
		updateSlateData.setMsTechnicalAssistanceInd(slateData.getMsTechnicalAssistanceInd());
		updateSlateData.setGrantFundingStatusCd(slateData.getGrantFundingStatusCd());
		
		updateSlateData.setApplicationComment(slateData.getApplicationComment());
		
		if(slateData.getApplicationComment() != null && !slateData.getApplicationComment().isEmpty()) {
			updateSlateData.setGrantFundingStatusCd("AP");
		}
		
		ApplicationInformationEntity appEntity = appRepository.save(updateSlateData);		
		return appEntity;
	}
}
