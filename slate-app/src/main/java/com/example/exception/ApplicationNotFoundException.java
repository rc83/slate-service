package com.example.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ApplicationNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = -4126346486711566038L;

	public ApplicationNotFoundException(Integer applicationId, Integer slateId, String applicationName,
			String prAwardNo, float requestedAmount, float recommendedAmount, float totalAllocatedAmount,
			String fundingStatus, String edProgramContact, String edProgramContactPhone, String edProgramContactEmail,
			String stateCd, Integer rankNumber, String highRiskCd, String granteeDuns, String applicationCompleteInd,
			String abstractCompleteInd, String msMonitoringReqInd, String msTechnicalAssistanceInd,
			String accsDataString, String applicationStatus, String narrativeReceivedInd, String applicationComment,
			String lateInd, String duplicateInd, String duplicateAward, String eligibleStatusCd, String ineligibleCatCd,
			String eligibilityStatusCmt) {
		super();
		this.applicationId = applicationId;
		this.slateId = slateId;
		this.applicationName = applicationName;
		this.prAwardNo = prAwardNo;
		this.requestedAmount = requestedAmount;
		this.recommendedAmount = recommendedAmount;
		this.totalAllocatedAmount = totalAllocatedAmount;
		this.fundingStatus = fundingStatus;
		this.edProgramContact = edProgramContact;
		this.edProgramContactPhone = edProgramContactPhone;
		this.edProgramContactEmail = edProgramContactEmail;
		this.stateCd = stateCd;
		this.rankNumber = rankNumber;
		this.highRiskCd = highRiskCd;
		this.granteeDuns = granteeDuns;
		this.applicationCompleteInd = applicationCompleteInd;
		this.abstractCompleteInd = abstractCompleteInd;
		this.msMonitoringReqInd = msMonitoringReqInd;
		this.msTechnicalAssistanceInd = msTechnicalAssistanceInd;
		this.accsDataString = accsDataString;
		this.applicationStatus = applicationStatus;
		this.narrativeReceivedInd = narrativeReceivedInd;
		this.applicationComment = applicationComment;
		this.lateInd = lateInd;
		this.duplicateInd = duplicateInd;
		this.duplicateAward = duplicateAward;
		this.eligibleStatusCd = eligibleStatusCd;
		this.ineligibleCatCd = ineligibleCatCd;
		this.eligibilityStatusCmt = eligibilityStatusCmt;
	}

	private Integer applicationId;
	private Integer slateId;
	private String applicationName;
	private String prAwardNo;
	private float requestedAmount;
	private float recommendedAmount;
	private float totalAllocatedAmount;
	private String fundingStatus;
	private String edProgramContact;
	private String edProgramContactPhone;
	private String edProgramContactEmail;
	private String stateCd;
	private Integer rankNumber;
	private String highRiskCd;
	private String granteeDuns;
	private String applicationCompleteInd;
	private String abstractCompleteInd;
	private String msMonitoringReqInd;
	private String msTechnicalAssistanceInd;
	private String accsDataString;
	private String applicationStatus;
	private String narrativeReceivedInd;
	private String applicationComment;
	private String lateInd;
	private String duplicateInd;
	private String duplicateAward;
	private String eligibleStatusCd;
	private String ineligibleCatCd;
	private String eligibilityStatusCmt;
	
	public Integer getApplicationId() {
		return applicationId;
	}

	public Integer getSlateId() {
		return slateId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public String getPrAwardNo() {
		return prAwardNo;
	}

	public float getRequestedAmount() {
		return requestedAmount;
	}

	public float getRecommendedAmount() {
		return recommendedAmount;
	}

	public float getTotalAllocatedAmount() {
		return totalAllocatedAmount;
	}

	public String getFundingStatus() {
		return fundingStatus;
	}

	public String getEdProgramContact() {
		return edProgramContact;
	}

	public String getEdProgramContactPhone() {
		return edProgramContactPhone;
	}

	public String getEdProgramContactEmail() {
		return edProgramContactEmail;
	}

	public String getStateCd() {
		return stateCd;
	}

	public Integer getRankNumber() {
		return rankNumber;
	}

	public String getHighRiskCd() {
		return highRiskCd;
	}

	public String getGranteeDuns() {
		return granteeDuns;
	}

	public String getApplicationCompleteInd() {
		return applicationCompleteInd;
	}

	public String getAbstractCompleteInd() {
		return abstractCompleteInd;
	}

	public String getMsMonitoringReqInd() {
		return msMonitoringReqInd;
	}

	public String getMsTechnicalAssistanceInd() {
		return msTechnicalAssistanceInd;
	}

	public String getAccsDataString() {
		return accsDataString;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public String getNarrativeReceivedInd() {
		return narrativeReceivedInd;
	}

	public String getApplicationComment() {
		return applicationComment;
	}

	public String getLateInd() {
		return lateInd;
	}

	public String getDuplicateInd() {
		return duplicateInd;
	}

	public String getDuplicateAward() {
		return duplicateAward;
	}

	public String getEligibleStatusCd() {
		return eligibleStatusCd;
	}

	public String getIneligibleCatCd() {
		return ineligibleCatCd;
	}

	public String getEligibilityStatusCmt() {
		return eligibilityStatusCmt;
	}

}
