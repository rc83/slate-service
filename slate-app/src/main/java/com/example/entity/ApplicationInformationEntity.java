package com.example.entity;

import javax.persistence.*;

@Entity
@Table(name="application_info")
public class ApplicationInformationEntity {
	
	public ApplicationInformationEntity() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
	private Integer applicationId;
	private Integer slateId;
	private String applicantName;
	private String prAwardNo;
	private Float requestedAmount;
	private Float recommendedAmount;
	private Float totalAllocatedAmount;
	private String grantFundingStatusCd;
	private String edProgramContact;
	private String edProgramContactPhone;
	private String edProgramContactEmail;
	private String stateCd;
	private Integer rankNo;
	private String highRiskCd;
	private String granteeDuns;
	private String applicationCompleteInd;
	private String abstractCompleteInd;
	private String msMonitoringReqInd;
	private String msTechnicalAssistanceInd;
	private String accsDataString;
	private String applicationStatus;
	private String narrativeReceivedInd;
	private String applicationComment;
	private String lateInd;
	private String duplicateInd;
	private String duplicateAward;
	private String eligibleStatusCd;
	private String ineligibleCatCd;
	private String eligibilityStatusCmt;
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getSlateId() {
		return slateId;
	}
	public void setSlateId(Integer slateId) {
		this.slateId = slateId;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getPrAwardNo() {
		return prAwardNo;
	}
	public void setPrAwardNo(String prAwardNo) {
		this.prAwardNo = prAwardNo;
	}
	public Float getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(float requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public Float getRecommendedAmount() {
		return recommendedAmount;
	}
	public void setRecommendedAmount(float recommendedAmount) {
		this.recommendedAmount = recommendedAmount;
	}
	public Float getTotalAllocatedAmount() {
		return totalAllocatedAmount;
	}
	public void setTotalAllocatedAmount(float totalAllocatedAmount) {
		this.totalAllocatedAmount = totalAllocatedAmount;
	}
	public String getGrantFundingStatusCd() {
		return grantFundingStatusCd;
	}
	public void setGrantFundingStatusCd(String grantFundingStatusCd) {
		this.grantFundingStatusCd = grantFundingStatusCd;
	}
	public String getEdProgramContact() {
		return edProgramContact;
	}
	public void setEdProgramContact(String edProgramContact) {
		this.edProgramContact = edProgramContact;
	}
	public String getEdProgramContactPhone() {
		return edProgramContactPhone;
	}
	public void setEdProgramContactPhone(String edProgramContactPhone) {
		this.edProgramContactPhone = edProgramContactPhone;
	}
	public String getEdProgramContactEmail() {
		return edProgramContactEmail;
	}
	public void setEdProgramContactEmail(String edProgramContactEmail) {
		this.edProgramContactEmail = edProgramContactEmail;
	}
	public String getStateCd() {
		return stateCd;
	}
	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}
	public Integer getRankNo() {
		return rankNo;
	}
	public void setRankNo(Integer rankNo) {
		this.rankNo = rankNo;
	}
	public String getHighRiskCd() {
		return highRiskCd;
	}
	public void setHighRiskCd(String highRiskCd) {
		this.highRiskCd = highRiskCd;
	}
	public String getGranteeDuns() {
		return granteeDuns;
	}
	public void setGranteeDuns(String granteeDuns) {
		this.granteeDuns = granteeDuns;
	}
	public String getApplicationCompleteInd() {
		return applicationCompleteInd;
	}
	public void setApplicationCompleteInd(String applicationCompleteInd) {
		this.applicationCompleteInd = applicationCompleteInd;
	}
	public String getAbstractCompleteInd() {
		return abstractCompleteInd;
	}
	public void setAbstractCompleteInd(String abstractCompleteInd) {
		this.abstractCompleteInd = abstractCompleteInd;
	}
	public String getMsMonitoringReqInd() {
		return msMonitoringReqInd;
	}
	public void setMsMonitoringReqInd(String msMonitoringReqInd) {
		this.msMonitoringReqInd = msMonitoringReqInd;
	}
	public String getMsTechnicalAssistanceInd() {
		return msTechnicalAssistanceInd;
	}
	public void setMsTechnicalAssistanceInd(String msTechnicalAssistanceInd) {
		this.msTechnicalAssistanceInd = msTechnicalAssistanceInd;
	}
	public String getAccsDataString() {
		return accsDataString;
	}
	public void setAccsDataString(String accsDataString) {
		this.accsDataString = accsDataString;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getNarrativeReceivedInd() {
		return narrativeReceivedInd;
	}
	public void setNarrativeReceivedInd(String narrativeReceivedInd) {
		this.narrativeReceivedInd = narrativeReceivedInd;
	}
	public String getApplicationComment() {
		return applicationComment;
	}
	public void setApplicationComment(String applicationComment) {
		this.applicationComment = applicationComment;
	}
	public String getLateInd() {
		return lateInd;
	}
	public void setLateInd(String lateInd) {
		this.lateInd = lateInd;
	}
	public String getDuplicateInd() {
		return duplicateInd;
	}
	public void setDuplicateInd(String duplicateInd) {
		this.duplicateInd = duplicateInd;
	}
	public String getDuplicateAward() {
		return duplicateAward;
	}
	public void setDuplicateAward(String duplicateAward) {
		this.duplicateAward = duplicateAward;
	}
	public String getEligibleStatusCd() {
		return eligibleStatusCd;
	}
	public void setEligibleStatusCd(String eligibleStatusCd) {
		this.eligibleStatusCd = eligibleStatusCd;
	}
	public String getIneligibleCatCd() {
		return ineligibleCatCd;
	}
	public void setIneligibleCatCd(String ineligibleCatCd) {
		this.ineligibleCatCd = ineligibleCatCd;
	}
	public String getEligibilityStatusCmt() {
		return eligibilityStatusCmt;
	}
	public void setEligibilityStatusCmt(String eligibilityStatusCmt) {
		this.eligibilityStatusCmt = eligibilityStatusCmt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((abstractCompleteInd == null) ? 0 : abstractCompleteInd.hashCode());
		result = prime * result + ((accsDataString == null) ? 0 : accsDataString.hashCode());
		result = prime * result + ((applicantName == null) ? 0 : applicantName.hashCode());
		result = prime * result + ((applicationComment == null) ? 0 : applicationComment.hashCode());
		result = prime * result + ((applicationCompleteInd == null) ? 0 : applicationCompleteInd.hashCode());
		result = prime * result + ((applicationId == null) ? 0 : applicationId.hashCode());
		result = prime * result + ((applicationStatus == null) ? 0 : applicationStatus.hashCode());
		result = prime * result + ((duplicateAward == null) ? 0 : duplicateAward.hashCode());
		result = prime * result + ((duplicateInd == null) ? 0 : duplicateInd.hashCode());
		result = prime * result + ((edProgramContact == null) ? 0 : edProgramContact.hashCode());
		result = prime * result + ((edProgramContactEmail == null) ? 0 : edProgramContactEmail.hashCode());
		result = prime * result + ((edProgramContactPhone == null) ? 0 : edProgramContactPhone.hashCode());
		result = prime * result + ((eligibilityStatusCmt == null) ? 0 : eligibilityStatusCmt.hashCode());
		result = prime * result + ((eligibleStatusCd == null) ? 0 : eligibleStatusCd.hashCode());
		result = prime * result + ((grantFundingStatusCd == null) ? 0 : grantFundingStatusCd.hashCode());
		result = prime * result + ((granteeDuns == null) ? 0 : granteeDuns.hashCode());
		result = prime * result + ((highRiskCd == null) ? 0 : highRiskCd.hashCode());
		result = prime * result + ((ineligibleCatCd == null) ? 0 : ineligibleCatCd.hashCode());
		result = prime * result + ((lateInd == null) ? 0 : lateInd.hashCode());
		result = prime * result + ((msMonitoringReqInd == null) ? 0 : msMonitoringReqInd.hashCode());
		result = prime * result + ((msTechnicalAssistanceInd == null) ? 0 : msTechnicalAssistanceInd.hashCode());
		result = prime * result + ((narrativeReceivedInd == null) ? 0 : narrativeReceivedInd.hashCode());
		result = prime * result + ((prAwardNo == null) ? 0 : prAwardNo.hashCode());
		result = prime * result + ((rankNo == null) ? 0 : rankNo.hashCode());
		result = prime * result + Float.floatToIntBits(recommendedAmount);
		result = prime * result + Float.floatToIntBits(requestedAmount);
		result = prime * result + ((slateId == null) ? 0 : slateId.hashCode());
		result = prime * result + ((stateCd == null) ? 0 : stateCd.hashCode());
		result = prime * result + Float.floatToIntBits(totalAllocatedAmount);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationInformationEntity other = (ApplicationInformationEntity) obj;
		if (abstractCompleteInd == null) {
			if (other.abstractCompleteInd != null)
				return false;
		} else if (!abstractCompleteInd.equals(other.abstractCompleteInd))
			return false;
		if (accsDataString == null) {
			if (other.accsDataString != null)
				return false;
		} else if (!accsDataString.equals(other.accsDataString))
			return false;
		if (applicantName == null) {
			if (other.applicantName != null)
				return false;
		} else if (!applicantName.equals(other.applicantName))
			return false;
		if (applicationComment == null) {
			if (other.applicationComment != null)
				return false;
		} else if (!applicationComment.equals(other.applicationComment))
			return false;
		if (applicationCompleteInd == null) {
			if (other.applicationCompleteInd != null)
				return false;
		} else if (!applicationCompleteInd.equals(other.applicationCompleteInd))
			return false;
		if (applicationId == null) {
			if (other.applicationId != null)
				return false;
		} else if (!applicationId.equals(other.applicationId))
			return false;
		if (applicationStatus == null) {
			if (other.applicationStatus != null)
				return false;
		} else if (!applicationStatus.equals(other.applicationStatus))
			return false;
		if (duplicateAward == null) {
			if (other.duplicateAward != null)
				return false;
		} else if (!duplicateAward.equals(other.duplicateAward))
			return false;
		if (duplicateInd == null) {
			if (other.duplicateInd != null)
				return false;
		} else if (!duplicateInd.equals(other.duplicateInd))
			return false;
		if (edProgramContact == null) {
			if (other.edProgramContact != null)
				return false;
		} else if (!edProgramContact.equals(other.edProgramContact))
			return false;
		if (edProgramContactEmail == null) {
			if (other.edProgramContactEmail != null)
				return false;
		} else if (!edProgramContactEmail.equals(other.edProgramContactEmail))
			return false;
		if (edProgramContactPhone == null) {
			if (other.edProgramContactPhone != null)
				return false;
		} else if (!edProgramContactPhone.equals(other.edProgramContactPhone))
			return false;
		if (eligibilityStatusCmt == null) {
			if (other.eligibilityStatusCmt != null)
				return false;
		} else if (!eligibilityStatusCmt.equals(other.eligibilityStatusCmt))
			return false;
		if (eligibleStatusCd == null) {
			if (other.eligibleStatusCd != null)
				return false;
		} else if (!eligibleStatusCd.equals(other.eligibleStatusCd))
			return false;
		if (grantFundingStatusCd == null) {
			if (other.grantFundingStatusCd != null)
				return false;
		} else if (!grantFundingStatusCd.equals(other.grantFundingStatusCd))
			return false;
		if (granteeDuns == null) {
			if (other.granteeDuns != null)
				return false;
		} else if (!granteeDuns.equals(other.granteeDuns))
			return false;
		if (highRiskCd == null) {
			if (other.highRiskCd != null)
				return false;
		} else if (!highRiskCd.equals(other.highRiskCd))
			return false;
		if (ineligibleCatCd == null) {
			if (other.ineligibleCatCd != null)
				return false;
		} else if (!ineligibleCatCd.equals(other.ineligibleCatCd))
			return false;
		if (lateInd == null) {
			if (other.lateInd != null)
				return false;
		} else if (!lateInd.equals(other.lateInd))
			return false;
		if (msMonitoringReqInd == null) {
			if (other.msMonitoringReqInd != null)
				return false;
		} else if (!msMonitoringReqInd.equals(other.msMonitoringReqInd))
			return false;
		if (msTechnicalAssistanceInd == null) {
			if (other.msTechnicalAssistanceInd != null)
				return false;
		} else if (!msTechnicalAssistanceInd.equals(other.msTechnicalAssistanceInd))
			return false;
		if (narrativeReceivedInd == null) {
			if (other.narrativeReceivedInd != null)
				return false;
		} else if (!narrativeReceivedInd.equals(other.narrativeReceivedInd))
			return false;
		if (prAwardNo == null) {
			if (other.prAwardNo != null)
				return false;
		} else if (!prAwardNo.equals(other.prAwardNo))
			return false;
		if (rankNo == null) {
			if (other.rankNo != null)
				return false;
		} else if (!rankNo.equals(other.rankNo))
			return false;
		if (Float.floatToIntBits(recommendedAmount) != Float.floatToIntBits(other.recommendedAmount))
			return false;
		if (Float.floatToIntBits(requestedAmount) != Float.floatToIntBits(other.requestedAmount))
			return false;
		if (slateId == null) {
			if (other.slateId != null)
				return false;
		} else if (!slateId.equals(other.slateId))
			return false;
		if (stateCd == null) {
			if (other.stateCd != null)
				return false;
		} else if (!stateCd.equals(other.stateCd))
			return false;
		if (Float.floatToIntBits(totalAllocatedAmount) != Float.floatToIntBits(other.totalAllocatedAmount))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ApplicationInformation [applicationId=" + applicationId + ", slateId=" + slateId + ", applicantName="
				+ applicantName + ", prAwardNo=" + prAwardNo + ", requestedAmount=" + requestedAmount
				+ ", recommendedAmount=" + recommendedAmount + ", totalAllocatedAmount=" + totalAllocatedAmount
				+ ", grantFundingStatusCd=" + grantFundingStatusCd + ", edProgramContact=" + edProgramContact
				+ ", edProgramContactPhone=" + edProgramContactPhone + ", edProgramContactEmail="
				+ edProgramContactEmail + ", stateCd=" + stateCd + ", rankNo=" + rankNo + ", highRiskCd=" + highRiskCd
				+ ", granteeDuns=" + granteeDuns + ", applicationCompleteInd=" + applicationCompleteInd
				+ ", abstractCompleteInd=" + abstractCompleteInd + ", msMonitoringReqInd=" + msMonitoringReqInd
				+ ", msTechnicalAssistanceInd=" + msTechnicalAssistanceInd + ", accsDataString=" + accsDataString
				+ ", applicationStatus=" + applicationStatus + ", narrativeReceivedInd=" + narrativeReceivedInd
				+ ", applicationComment=" + applicationComment + ", lateInd=" + lateInd + ", duplicateInd="
				+ duplicateInd + ", duplicateAward=" + duplicateAward + ", eligibleStatusCd=" + eligibleStatusCd
				+ ", ineligibleCatCd=" + ineligibleCatCd + ", eligibilityStatusCmt=" + eligibilityStatusCmt + "]";
	}
	
	
	
}